# A simple operator to deploy kratos

See `config/samples` for details on the CRD.

## Deploy

```bash
git clone https://gitlab.com:faasaf/kratos-helm-operator.git
cd kratos-helm-operator
make deploy
```
